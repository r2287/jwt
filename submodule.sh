#/bin/sh

git submodule update --init --recursive; \
git pull --recurse-submodule; \
git submodule update --remote --recursive