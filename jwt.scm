(module jwt 
(jwtencode jwtdecode)
(import 
  scheme 
  (chicken base)
  (chicken string)
  (chicken format)
  (chicken port)
  (chicken condition)
  srfi-180
  hmac
  sha256-primitive
  base64url
  eshumlib)

(define (b64 s)
  (b64encode s))

;> (alist payload) -> string secret -> (alist headers) -> string
(define (jwtencode payload secret #!key [headers '()])
  (define alistheader (cons `(alg . "HS256") (cons '(typ . "JWT") headers)))
  (define header (alist->json alistheader))
  (define b64header (b64 header))
  (define jsonpayload (alist->json payload))
  (define b64payload (b64 jsonpayload))
  (define first-part (conc b64header "." b64payload))
  (define signature (b64 ((hmac secret (sha256-primitive)) first-part)))
  
  (conc b64header "." b64payload "." signature))

;> string -> (alist payload) | #f
(define (jwtdecode token #!key [secret ""])
  (define (exception-handler break)
    (define first-dot (substring-index "." token))

    (define (decode break)
      (define splitted (string-split token "."))
      (when (> (length splitted) 3) (break #f))
      (define jsonheader 
        (with-input-from-string (b64decode (car splitted)) json-read))
      (define algorithm (assoc-value 'alg jsonheader))
      (when (not (and algorithm (string-ci=? algorithm "HS256"))) (break #f))
      (define payload 
        (with-input-from-string (b64decode (cadr splitted)) json-read))
      
      (define first-part 
        (conc (b64encode (alist->json jsonheader)) "." 
              (b64encode (alist->json payload))))
      (define signature (b64encode ((hmac secret (sha256-primitive)) first-part)))
      `((data . ,(list payload)) (valid . ,(string=? signature (caddr splitted)))))

    (handle-exceptions exn (begin (print-error-message exn) (break #f))
      (if (and first-dot (> (string-length token) (+ 1 first-dot)))
          (call/cc decode)
          #f)))

  (if (string? token) (call/cc exception-handler) #f))
)